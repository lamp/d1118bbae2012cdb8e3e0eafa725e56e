var nmpb_id        = "561738578bfc6a71d8c9a907";
var output_channel = "lobby";
var input_channel  = "NMPB Autoplayer (Sockets)"; // damn Prince Fluff in Troll Heaven2
var my_name        = "NMPB (Ray890)*";
var mppUri         = "ws://www.multiplayerpiano.com:8080";

var Client = require('mpp-client');

var client1 = new Client(mppUri);
client1.setChannel(output_channel);
client1.start();
var client2 = new Client(mppUri);
client2.setChannel(input_channel);
client2.start();

client1.on("hi", function(){
	client1.sendArray([{m:'userset', set:{name:my_name}}]);
});

setInterval(()=>{
	if ((client1.isConnected() && !client1.channel) || (client1.channel && client1.channel._id != output_channel)) client1.setChannel(output_channel);
	if ((client2.isConnected() && !client2.channel) || (client2.channel && client2.channel._id != input_channel )) client2.setChannel(input_channel );
}, 1000);


// NMPB notes to lobby
client2.on('n', msg => {
    if (client2.findParticipantById(msg.p)._id != nmpb_id) return;
    client1.sendArray([msg]);
});
// NMPB chat to lobby
client2.on('a', msg => {
    if (msg.p._id != nmpb_id) return;
    client1.sendArray([{m:'a', message: msg.a}]);
});
// NMPB mouse to lobby
client2.on('m', msg => {
	if (client2.findParticipantById(msg.id)._id != nmpb_id) return;
	client1.sendArray([msg]);
});


// lobby commands to NMPB
var NMPB_commands = `
help h list search s title color info about
upload u
auto a play p random r restart rr set pos sustain
backwards b mistakes tempo speed inverse mirror octave oct echo transpose tp
`.trim().split('\n').join(' ').split(' ');
client1.on('a', msg => {
    if (!msg.a.startsWith('/')) return;
    if (!NMPB_commands.includes(msg.a.split(' ')[0].substr(1))) return;
    client2.sendArray([{m:'a', message: msg.a}]);
});